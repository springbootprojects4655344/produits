package com.bfolio.produits.services;

import com.bfolio.produits.dto.ProduitDTO;
import com.bfolio.produits.entities.Categorie;
import com.bfolio.produits.entities.Produit;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProduitService {

    ProduitDTO saveProduit(ProduitDTO p);
    ProduitDTO getProduit(Long id);
    List<ProduitDTO> getAllProduits();

    ProduitDTO updateproduit(ProduitDTO p);
    void deleteProduit(Produit p);
    void deleteProduitById(Long id);
    List<Produit> findByNomProduit(String nom);
    List<Produit> findByNomProduitContains(String nom);
    List<Produit> findByNomprix(@Param("nom") String nom, @Param("prix") Double prix);
    List<Produit> findByCategorie(@Param("categorie") Categorie categorie);
    List<Produit> findByCategorieIdCat(Long id);
    List<Produit> findByOrderByNomProduitAsc();
    List<Produit> trierProduitsNomPrix();

    ProduitDTO convertProduitToDto(Produit p);
    Produit convertDtoToProduit(ProduitDTO p);

}
