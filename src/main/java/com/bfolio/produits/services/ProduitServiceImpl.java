package com.bfolio.produits.services;

import com.bfolio.produits.dto.ProduitDTO;
import com.bfolio.produits.entities.Categorie;
import com.bfolio.produits.entities.Produit;
import com.bfolio.produits.repos.ProduitRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProduitServiceImpl implements ProduitService{

    @Autowired
     ProduitRepository produitRepository;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public ProduitDTO saveProduit(ProduitDTO p) {
        return convertProduitToDto(produitRepository.save(convertDtoToProduit(p)));
    }

    @Override
    public ProduitDTO updateproduit(ProduitDTO p) {
        return convertProduitToDto(produitRepository.save(convertDtoToProduit(p)));
    }

    @Override
    public void deleteProduit(Produit p) {
        produitRepository.delete(p);
    }

    @Override
    public void deleteProduitById(Long id) {
        produitRepository.deleteById(id);
    }

    @Override
    public ProduitDTO getProduit(Long id) {
        return convertProduitToDto(produitRepository.findById(id).get());
    }

    @Override
    public List<ProduitDTO> getAllProduits() {
        return produitRepository.findAll().stream()
                .map(this::convertProduitToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Produit> findByNomProduit(String nom) {
        return produitRepository.findByNomProduit(nom);
    }

    @Override
    public List<Produit> findByNomProduitContains(String nom) {
        return produitRepository.findByNomProduitContains(nom);
    }

    @Override
    public List<Produit> findByNomprix(String nom, Double prix) {
        return produitRepository.findByNomprix(nom, prix);
    }

    @Override
    public List<Produit> findByCategorie(Categorie categorie) {
        return produitRepository.findByCategorie(categorie);
    }

    @Override
    public List<Produit> findByCategorieIdCat(Long id) {
        return produitRepository.findByCategorieIdCat(id);
    }

    @Override
    public List<Produit> findByOrderByNomProduitAsc() {
        return produitRepository.findByOrderByNomProduitAsc();
    }

    @Override
    public List<Produit> trierProduitsNomPrix() {
        return produitRepository.trierProduitsNomPrix();
    }

    @Override
    public ProduitDTO convertProduitToDto(Produit p) {

        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        ProduitDTO produitDTO = modelMapper.map(p, ProduitDTO.class);
        return  produitDTO;


//        ProduitDTO produitDTO = new ProduitDTO();
//        produitDTO.setIdProduit(p.getIdProduit());
//        produitDTO.setNomProduit(p.getNomProduit());
//        produitDTO.setPrixProduit(p.getPrixProduit());
//        produitDTO.setCategorie(p.getCategorie());



//        return ProduitDTO.builder()
//                .idProduit(p.getIdProduit())
//                .nomProduit(p.getNomProduit())
////                .prixProduit(p.getPrixProduit())
//                .dateCreation(p.getDateCreation())
//                .categorie(p.getCategorie())
////                .nomCat(p.getCategorie().getNomCat())
//                .build();
    }

    @Override
    public Produit convertDtoToProduit(ProduitDTO produitDTO) {
        Produit produit = new Produit();
        produit = modelMapper.map(produitDTO, Produit.class);

        return produit;



//        Produit produit = new Produit();
//        produit.setIdProduit(produitDTO.getIdProduit());
//        produit.setNomProduit(produitDTO.getNomProduit());
//        produit.setPrixProduit(produitDTO.getPrixProduit());
//        produit.setDateCreation(produitDTO.getDateCreation());
//        produit.setCategorie(produitDTO.getCategorie());
//
//        return produit;
    }
}
