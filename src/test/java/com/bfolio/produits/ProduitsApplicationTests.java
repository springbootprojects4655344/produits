package com.bfolio.produits;

import com.bfolio.produits.entities.Categorie;
import com.bfolio.produits.entities.Produit;
import com.bfolio.produits.repos.ProduitRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
class ProduitsApplicationTests {
//
//    @Autowired
//    private ProduitRepository produitRepository;
//
//    @Test
//    public void testCreateProduit() {
//        Produit prod = new Produit("Poste SAMSUNG", 3000.200, new Date());
//        produitRepository.save(prod);
//    }
//
//    @Test
//    public void testFindProduit() {
//        Produit p = produitRepository.findById(1L).get();
//        System.out.println(p);
//    }
//
//    @Test
//    public void testUpdateProduit() {
//        Produit p = produitRepository.findById(1L).get();
//        p.setPrixProduit(2000.0);
//        produitRepository.save(p);
//        System.out.println(p);
//    }
//
//    @Test
//    public void testDeleteProduit() {
//        produitRepository.deleteById(2L);
//    }
//
//    @Test
//    public void testFindAllProduits() {
//        List<Produit> produits = produitRepository.findAll();
//        for (Produit p:produits) {
//            System.out.println(p);
//        }
//    }
//
//    @Test
//    public void testFindByNomProduit() {
//        List<Produit> prods = produitRepository.findByNomProduit("PC Dell");
//        for (Produit p: prods) {
//            System.out.println(p);
//        }
//    }
//
//    @Test
//    public void testFindByNomProduitContains() {
//        List<Produit> prods = produitRepository.findByNomProduitContains("PC");
//        for (Produit p: prods) {
//            System.out.println(p);
//        }
//    }
//
//    @Test
//    public void testfindByNomPrix() {
//        List<Produit> prods = produitRepository.findByNomprix("PC Dell", 1000.0);
//        for (Produit p: prods) {
//            System.out.println(p);
//        }
//    }
//
//    @Test
//    public void testFindByCategorie() {
//        Categorie cat = new Categorie();
//        cat.setIdCat(1L);
//
//        List<Produit> prods = produitRepository.findByCategorie(cat);
//
//        for (Produit p: prods) {
//            System.out.println(p);
//        }
//    }
//
//    @Test
//    public void testFindByCategorieIdCat() {
//        List<Produit> prods = produitRepository.findByCategorieIdCat(1L);
//
//        for (Produit p: prods) {
//            System.out.println(p);
//        }
//    }
//
//    @Test
//    public void testFindByOrderByNomProduitAsc() {
//        List<Produit> prods = produitRepository.findByOrderByNomProduitAsc();
//
//        for (Produit p: prods) {
//            System.out.println(p);
//        }
//    }
//
//    @Test
//    public void testTrierProdutsNomPrix() {
//        List<Produit> prods = produitRepository.trierProduitsNomPrix();
//
//        for (Produit p: prods) {
//            System.out.println(p);
//        }
//    }
}
